#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  7 20:54:21 2016

@author: eric
"""

import scipy.optimize as opt

if IPRINT == 1:
    disp = True
else:
    disp = False

if METHOD == 0 or METHOD == 1:
    meth = 'COBYLA'
elif METHOD == 2:
    meth= 'slsqp'

cons =({'type': 'ineq', 'fun': lambda x:  x[0] - 2 * x[1] + 2},
        {'type': 'ineq', 'fun': lambda x: -x[0] - 2 * x[1] + 6},
         {'type': 'ineq', 'fun': lambda x: -x[0] + 2 * x[1] + 2})
bnds = ((0, None), (0, None))

opts = {'iprint': 1, 'disp': disp, 'maxiter': 100}

res = opt.minimize(fun,x0,args=(), method = meth, bounds=bnds,constraints=cons, options=opts)