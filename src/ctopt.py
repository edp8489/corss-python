#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
file: ctopt.py

Created on Sun Nov  6 19:35:50 2016

@author: eric
"""

#include "ct.h"
#import ct
#include "eval.h"
import eval
#include "nlopt_interface.h"
import math
import numpy as np

def CALLeval( S, R, M, L, C,  G,  X, Xinit, OBJ, out, SSP, hap, l2ap, Nstap, tap, tstap, Wap, mflag, LEB):
    S.h = X[hap]*Xinit[hap];
    S.l2 = X[l2ap]*Xinit[l2ap];
    S.N = X[Nstap]*Xinit[Nstap];
    S.t = X[tstap]*Xinit[tstap];
    C.t = X[tap]*Xinit[tap];
    S.W = X[Wap]*Xinit[Wap];
    eval(S,R,M,L,C,G, OBJ,out,SSP,mflag,LEB);
# /******************************************* end CALLeval ****/


# return OBJ, METHOD, IPRINT, NDV, NCON
def ctopt(SSP, out, X, Xinit, hap, l2ap, tstap, Nstap, tap, Wap, S, R, M, L, C, G, OBJ, METHOD, IPRINT, NDV, NCON, XL, XU, mflag, LEB):
    #long int INFO, IPRM[20],IWK[110],j,MINMAX,NRIWK,NRWK;
    IPRM = np.zeros(20,dtype=int)
    IWK = np.zeros(110,dtype=int)
    RPRM = np.zeros(20,dtype=float)
    WK = np.zeros(450,dtype=float);
    #int iter, i;

    NRWK = 450; # make the dimension of IWK and WK the same 
    NRIWK = 110;
    MINMAX = -1;
    INFO = 0;
    if ( (SSP == 2) or (SSP == 3) ):
        print("\n\n OPTIMIZATION HISTORY\n\n",file=out);
        print("h    12    tst    Nst    t    W    skin    shell ",file=out);
        print("stres    Crip1    Crip2    Wt   It",file=out);
    # end if
    iter = 0;
    
    while True:
        if (IPRM[18]!= -1):
            print("\nIteration #%i",IPRM[18]);
            iter= int(IPRM[18]);
        if ( (SSP ==2) or (SSP ==3) ):
            print("\n%5.4f %5.4f %5.4f %5.1f %5.4f %5.4f ", X[hap]*Xinit[hap], X[l2ap]*Xinit[l2ap], X[tstap]*Xinit[tstap], X[Nstap]*Xinit[Nstap],X[tap]*Xinit[tap], X[Wap]*Xinit[Wap],file=out);
            out.flush();
         #end if
        CALLeval(S,R,M,L,C,G,X, Xinit,OBJ,out,SSP,hap, l2ap,Nstap,tap,tstap,Wap,mflag,LEB);
        for i in range(0,NCON):
            G[i] += .005;
        """ 
        here is where we need to replace DOT with the proper scipy.optimize routine
        
        DOT(&INFO,METHOD, IPRINT,NDV, NCON, X, XL,XU,OBJ,&MINMAX,G, RPRM, IPRM,
            WK,&NRWK, IWK,&NRIWK);
        
        """
        print(" %3.1f %7.4f %7.4f %7.4f %7.4f %7.4f",OBJ,G[0],G[1],G[2],G[3],G[4]);
        if ( (SSP == 2) or (SSP == 3) ):
            print('%5.2f %5.2f %5.2f %5.2f %5.2f',G[0],G[1],G[2],G[3],G[4],file=out);
            print(" %4.0f %d",OBJ, iter,file=out);
        # end if
        if not (INFO!=0):
            break
    #/*** end optimization (INFO=0) loop ***/
    print("\n\nNumber of Iterations to Optimize = %d",iter,file=out);
    if (iter > 20):
        print(" -- WARNING -- Solution may not have converged.",file=out);
# /******************************************************** end ctopt ****/

