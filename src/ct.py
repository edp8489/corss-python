#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Nov  5 00:13:45 2016

@author: eric
"""

class Stiffness():
     def __init__(self,  Ex=None, Ey=None, Exy=None, Gxy=None, Dx=None, Dy=None, Dxy=None, Cx=None, Cy=None, Cxy=None, Kxy=None):
         #Ex = 0.0
         self.Ex =  Ex if Ex is not None else 0.0
         #Ey = 0.0
         self.Ey =  Ey if Ey is not None else 0.0
         #Exy = 0.0
         self.Exy =  Exy if Exy is not None else 0.0
         #Gxy = 0.0
         self.Gxy =  Gxy if Gxy is not None else 0.0
         #Dx = 0.0
         self.Dx =  Dx if Dx is not None else 0.0
         #Dy = 0.0
         self.Dy =  Dy if Dy is not None else 0.0
         #Dxy = 0.0
         self.Dxy =  Dxy if Dxy is not None else 0.0
         #Cx = 0.0
         self.Cx =  Cx if Cx is not None else 0.0
         #Cy = 0.0
         self.Cy =  Cy if Cy is not None else 0.0
         #Cxy = 0.0
         self.Cxy =  Cxy if Cxy is not None else 0.0
         #Kxy = 0.0
         self.Kxy =  Kxy if Kxy is not None else 0.0
 # end stiffness class
 
class Material():
    def __init__(self, E=None, nu=None, rho=None, Scy=None, G=None, Stu=None):
         self.E =  E if E is not None else 0.0
         self.nu =  nu if nu is not None else 0.0
         self.rho =  rho if rho is not None else 0.0
         self.Scy =  Scy if Scy is not None else 0.0
         self.G =  G if G is not None else 0.0
         self.Stu =  Stu if Stu is not None else 0.0
 # end Material class

class Load():
    def __init__(self, F=None, M=None, Pa=None, Ph=None, sf=None, sfp=None, V=None):
         self.F =  F if F is not None else 0.0
         self.M =  M if M is not None else 0.0
         self.Pa =  Pa if Pa is not None else 0.0
         self.Ph =  Ph if Ph is not None else 0.0
         self.sf =  sf if sf is not None else 0.0
         self.sfp =  sfp if sfp is not None else 0.0
         self.V =  V if V is not None else 0.0
#end load class

class Stringer():
    def __init__(self, A=None, I=None, J=None, N=None, Z=None, alp=None, l1=None, MZs=None, h=None, l2=None, t=None, W=None, b=None, la=None, stype=None):
         self.A =  A if A is not None else 0.0
         self.I =  I if I is not None else 0.0
         self.J =  J if J is not None else 0.0
         self.N =  N if N is not None else 0.0
         self.Z =  Z if Z is not None else 0.0
         self.alp =  alp if alp is not None else 0.0
         self.l1 =  l1 if l1 is not None else 0.0
         self.MZs =  MZs if MZs is not None else 0.0
         self.h =  h if h is not None else 0.0
         self.l2 =  l2 if l2 is not None else 0.0
         self.t =  t if t is not None else 0.0
         self.W =  W if W is not None else 0.0
         self.b =  b if b is not None else 0.0
         self.la =  la if la is not None else 0.0
         self.stype =  stype if stype is not None else ''
# end stringer class

class Ring():
    def __init__(self, A=None, I=None, J=None, N=None, Z=None,d=None):
         self.A =  A if A is not None else 0.0
         self.I =  I if I is not None else 0.0
         self.J =  J if J is not None else 0.0
         self.N =  N if N is not None else 0.0
         self.Z =  Z if Z is not None else 0.0
         self.d =  d if d is not None else 0.0
# end Ring class


class Cylinder():
    def __init__(self, fwt=None, l=None, r=None, t=None):
         self.fwt =  fwt if fwt is not None else 0.0
         self.l =  l if l is not None else 0.0
         self.r =  r if r is not None else 0.0
         self.t =  t if t is not None else 0.0
 # end cylinder class

 # return [NDV,hap,l2ap,tstap,Nstap,tap,Wap]
def initDVs(XL, X, XU, hap, l2ap,tstap, Nstap, tap, Wap, Xinit, ndv):
    #int i;
    NDV = ndv;
    pos = 0;
    if ( (XL[pos] == X[pos]) or (X[pos] == XU[pos]) ):
        hap = Xadjust(6,pos,NDV, X,Xinit,XL,XU);
    else:
        hap = pos + 1
    # end if

    if ( (XL[pos] == X[pos]) and (X[pos] == XU[pos]) ):
        l2ap = Xadjust(7,pos,NDV, X,Xinit,XL,XU);
    else:
        l2ap = pos + 1;
    # end if

    if ( (XL[pos] == X[pos]) and (X[pos] == XU[pos]) ):
        tstap = Xadjust(8,pos,NDV, X,Xinit,XL,XU);
    else:
        tstap = pos + 1
    # end if

    if ( (XL[pos]==X[pos]) and (X[pos]==XU[pos])):
        Nstap = Xadjust(9,pos,NDV, X,Xinit,XL,XU);
    else:
        Nstap = pos + 1
    # end if

    if ( (XL[pos]==X[pos]) and (X[pos]==XU[pos])):
        tap = Xadjust(10,pos,NDV, X, Xinit,XL,XU);
    else:
        tap = pos + 1
    # end if

    if ((0==XL[pos]) and (0==X[pos]) and (0==XU[pos])):
        Wap = tstap
        NDV -= 1
    else:
        if ( (XL[pos]==X[pos]) and (X[pos]==XU[pos])):
            Wap = Xadjust(11,pos,NDV, X,Xinit,XL,XU);
        else:
            Wap = pos;
        #end if
    # end if
    
    for i in range(0,NDV):
        Xinit[i] = X[i];
        XL[i] = XL[i]/Xinit[i];
        XU[i] = XU[i]/Xinit[i];
        X[i] = 1.0;
    # end for
    #return (NDV);
    return [NDV,hap,l2ap,tstap,Nstap,tap,Wap]
# /************************************** end initDVs *******/


def comlineerr():
    print("\nUsage: C:\>CORSS  file.in  file.out");
    #fcloseall();
    exit(1);
# end


def Xadjust(newpos, pos, NDV, X,  Xinit, XL, XU):
    #int k;
    X[newpos] = X[pos];
    Xinit[newpos] = 1.;
    for k in range(pos,NDV-1):
        XL[k] = XL[k+1];
        X[k] = X[k+1];
        XU[k]=XU[k+1]; 
    # end for
    NDV -= 1;
    return [newpos, NDV]
# end Xadjust 