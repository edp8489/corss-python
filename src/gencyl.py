#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov  4 22:41:14 2016

@author: eric
"""
import math
import ct
#include "ct.h"

def gammaF(ES, r):
    return (1-.901*(1-math.exp(-math.sqrt(r/pow(ES.Dx*ES.Dy/ES.Ex/ES.Ey,.25))/29.8)));
# end

def gammaM(ES, r):
    return (1-.731*(1-math.exp(-math.sqrt(r/pow(ES.Dx*ES.Dy/ES.Ex/ES.Ey,.25))/29.8)));


def getstiffnesses(R, S, M, C, egdck, out, SSP):
    #float Er, Es, Gr, Gs;
    Es = M.E;
    Er = M.E;
    Gs = M.G;
    Gr = M.G;
    if (0 == R.N):
        R.d = 1.0E35;
    egdck.Ex = M.E*C.t/(1-M.nu*M.nu) + Es*S.A/S.b;
    egdck.Ey = M.E*C.t/(1-M.nu*M.nu) + Er*R.A/R.d;
    egdck.Exy = M.nu*M.E*C.t/(1-M.nu*M.nu) ;
    egdck.Gxy = M.E*C.t/2/(1+M.nu) ;
    egdck.Dx = M.E*C.t*C.t*C.t/12/(1-M.nu*M.nu) + Es*S.I/S.b + S.Z*S.Z*Es*S.A/S.b;
    egdck.Dy = M.E*C.t*C.t*C.t/12/(1-M.nu*M.nu) + Er*R.I/R.d + R.Z*R.Z*Er*R.A/R.d;
    egdck.Dxy = M.E*C.t*C.t*C.t/6/(1+M.nu) + Gs*S.J/S.b + Gr*R.J/R.d;
    egdck.Cx = S.Z*Es*S.A/S.b;
    egdck.Cy = R.Z*Er*R.A/R.d;
    egdck.Cxy = C.t*C.t*C.t/12/(1-M.nu*M.nu);
    egdck.Kxy = egdck.Cxy;
    if (SSP == 4):
        print('\n\nEx = %14.4f  Ey = %14.4f  Exy = %14.4f',egdck.Ex, egdck.Ey,egdck.Exy,file=out);
        print('\nDx = %14.4f   Dy = %14.4f   Dxy = %14.4f',egdck.Dx, egdck.Dy,egdck.Dxy,file=out);
        print('\nCx = %14.4f   Cy = %14.4G   Cxy = %14.4G',egdck.Cx, egdck.Cy,egdck.Cxy,file=out);
        print('\nGxy = %14.4f  Kxy = %14.4G',egdck.Gxy,egdck.Kxy,file=out);
    
    return egdck
# /************************** end getstiffnesses ****/

def getA(ES, r, l, m, n,out, SSP):
    #float A11,A22,A33,A12,A21,A23,A32,A31,A13,detA3x3,detA2x2;
    A11 = ES.Ex*m*m*math.pi*math.pi/l/l + ES.Gxy*n*n/r/r;
    A22 = ES.Ey*n*n/r/r + ES.Gxy*m*m*math.pi*math.pi/l/l;
    
    A33 = ES.Dx*pow( m*math.pi/l , 4 ) + ES.Dxy*pow( m*math.pi*n/l/r , 2 ) 
    + ES.Dy*pow( n/r , 4 ) + ES.Ey/r/r + 2*ES.Cy/r*n*n/r/r 
    + 2*ES.Cxy/r*m*m*math.pi*math.pi/l/l;
    
    A12= (ES.Exy+ES.Gxy)*m*math.pi/l*n/r;
    A21 = A12;
    
    A23 = (ES.Cxy + 2*ES.Kxy)*m*m*math.pi*math.pi/l/l*n/r + ES.Ey*n/r/r 
    + ES.Cy*pow( n/r , 3 );
    
    A32 = A23;
    A31 = ES.Exy/r*m*math.pi/l + ES.Cx*pow(m*math.pi/l,3) 
    + (ES.Cxy+2*ES.Kxy)*m*math.pi/l*n*n/r/r;
    
    A13 = A31;
    
    detA3x3 = A11*A22*A33 + A12*A23*A31 + A13*A21*A32
    - A13*A22*A31 - A11*A23*A32 - A12*A21*A33;
    
    detA2x2 = A11*A22 - A12*A21;
    if (SSP == 6):
        print('\n\n A = | %12.4f    %12.4f    %12.4f |',A11,A12,A13,file=out);
        print('\n      | %12.4f    %12.4f    %12.4f |',A21,A22,A23,file=out);
        print('\n      | %12.4f    %12.4f    %12.4f |',A31,A32,A33,file=out);
        print('\n\ndeterminant of A(3x3) = %G',detA3x3,file=out);
        print('\ndeterminant of A(2x2) = %G',detA2x2,file=out);
    
    return( detA3x3/detA2x2 )
# /******* end getA ****/


def Pcrcalc(egdck, r, l, ncr, out, SSP):
    #float P, Pcr, Pold;
    #int n;
    Pcr = .75*r/l/l*getA(egdck,r,l,1,1,out,SSP);
    for n in range(2,150):
        P = .75*r/n/n*getA(egdck,r,l,1,n,out,SSP)
        if ( P  < Pcr ):
            Pcr = P
            ncr = n;
        #end if
    #end for
    if (SSP == 4):
        print('\n\nCritical Pressure',file=out);
        P = getA(egdck, r,l,1,ncr,out,6);
        print("\nCritical Pressure = %6.2f with %i circumferential waves",Pcr,ncr,file=out);
        if ( ncr >= 149 ):
            print("\nWARNING: Program may not have calculated lowest ",file=out);
            print("critical pressure, ncr may be greater than %d",ncr,file=out);
        # end if
    #end if
    return [Pcr, ncr]; #return ncr
# /************************************************* end Pcrcalc */




def gencyl(l, Nr, r,  egdck, mmin, nmin, Nx, out, SSP):
    #float Nm, Nold,Nmt;
    #int m, n,mm, nm;
    m = 1;
    n = 1;
    mm = 1;
    nm = 1;
    mmin = 1;
    nmin = 1;
    Nm = l*l/m/m/math.pi/math.pi*getA(egdck, r,l,m,n,out,SSP);
    Nold = Nm*2;
    Nx = Nm;
    while ((Nx) < Nold):
        Nold = (Nx);
        n=4;
        Nm = l*l/m/m/math.pi/math.pi*getA(egdck, r,l,m,n,out,SSP);
        #do
        while True:
            Nmt = l*l/m/m/math.pi/math.pi*getA(egdck, r,l,m,n,out,SSP)
            if ( Nmt < Nm ):
                Nm = Nmt
                mm = m
                nm = n
            n += 1
            if not (Nm == Nmt):
                break
        if (Nm < (Nx) ):
            Nx = Nm
            mmin = mm
            nmin = nm
        m += 1
    #end while
    if (SSP == 4):
        print('\n\nCritical Line Load, m=%d, n=%d, Ncr=%f',mmin,nmin,Nx,file=out);
        Nmt = getA(egdck, r,l,mmin,nmin,out,6);
    #end if
    if ( ((SSP==4) or (SSP==5)) and ((mmin != 1) and (m == Nr+1)) ):
        print('\n\nWARNING! for the critical load case, there is one',file=out);
        print('axial half wave\nper section between rings. The rings',file=out);
        print('may not help\nsupport general cylinder buckling.',file=out);
    #end if
    return [mmin, nmin, Nx]
# /************************************************* end gencyl */



def GCBcalc(R, S, M, C, L, G, out, SSP,mgcb, ngcb, Nx, Pcr, N, Io, Faxial, ncr, GCB, gamF, gamM):
    #struct stiffness egdck;
    egdck = ct.Stiffness()
    G1 = 1000.
    print('G');
    egdck = getstiffnesses(R,S,M,C,egdck,out,SSP);
    if ((G[0]<0) or (L.sf<=L.sfp)): #/* if skin doesn't buckle */
        [mmin, nmin, Nx] = gencyl(C.l,R.N,C.r,egdck,mgcb, ngcb, Nx,out,SSP);
        gamM = gammaM(egdck,C.r);
        gamF = gammaF(egdck, C.r);
        N = (L.M*L.sf*C.r/Io*(S.A/S.b+C.t))/(gamM)+ Faxial*(S.A/S.b+C.t)/(gamF);
        if (4==SSP):
        # end if
            print("\n\nCritical line load is Ncr = %f",Nx,file=out);
            print('\nKnockdown Adjusted Line Load is %f',N,file=out);
            print('\nat axial waves m = %d, and hoop waves n = %d',mgcb,ngcb,file=out);
            print('\ngammaF = %f, gammaM = %f',gamF,gamM,file=out);
        # end if
        G1 = (N)/(Nx) - 1;
        if (L.Ph < 0.0):
            [Pcr,ncr] = Pcrcalc(egdck,C.r,C.l,ncr,out,SSP);
            G1 += -L.Ph*L.sf/(Pcr);
            if (4==SSP):
                print('\n\nCritical crushing pressure Pcr = %f',Pcr,file=out);
        # end if
        if (G1<G[1]):
            G[1] = G1;
            GCB = 'Y';
            if (4 == SSP):
                print('\nGeneral Cylinder Buckling provides support above Column Buckling',file=out);
            # end if    
        # end if
        else:
            if (4==SSP):
                print('\nColumn Buckling provides support above General Cylinder Buckling',file=out);
        return ( G[1]);
    # end if
    if (4==SSP):
        print('\nGeneral Cylinder Buckling not checked because of',file=out);
        print(' buckled skin',file=out);
    # end if
    #return (G[1]);
    return [G[1], mgcb, ngcb, Nx, Pcr, N, ncr, GCB, gamF, gamM]
# /*********************** end GCBcalc ****/








