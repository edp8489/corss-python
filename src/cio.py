#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
file cio.py

Created on Sun Nov  6 20:23:59 2016

@author: eric

this entire file should be rewritten to have the inputs specified as JSON
and marshaled into their respective variables


"""
import json
from pprint import pprint
import math
#include "ct.h"

def parse_json_input(inputfile):
    with open(inputfile) as input:
        input_values = json.load(input)
    pprint(input_values)
    
    return input_values
# end parse_json_input


# return [in, IPRINT, METHOD, SSP, M, C, S, R, L, mflag, LEB]
def readinput(Title, in_file, IPRINT, METHOD, SSP, M, C, S, R, L, XL, X, XU, Tol, mflag, LEB):
    # parse the input file and get a dictionary of values
    inputs = parse_json_input(in_file)
    
    Title = inputs["Title"]
    
    IPRINT = inputs["Opt-Flags"]["IPRINT"]
    METHOD = inputs["Opt-Flags"]["METHOD"]
    SSP = inputs["SSP"]
    
    M.nu = inputs["Material"]["nu"]
    M.E = inputs["Material"]["E"]
    M.G = inputs["Material"]["SM"]
    M.rho = inputs["Material"]["rho"]
    M.Stu = inputs["Material"]["Stu"]
    M.Scy = inputs["Material"]["Scy"]
    
    C.r = inputs["Cylinder"]["r"]
    C.l = inputs["Cylinder"]["l"]
    C.fwt = inputs["Cylinder"]["fwt"]
    
    S.stype = inputs["Stringer"]["stype"]
    LEB = inputs["Stringer"]["LEB"]
    mflag = inputs["Stringer"]["mflag"]
    S.alp = inputs["Stringer"]["alp"]
    S.l1 = inputs["Stringer"]["l1"]
    S.MZs = inputs["Stringer"]["MZs"]
    
    R.A = inputs["Ring"]["Ar"]
    R.I = inputs["Ring"]["Ir"]
    R.Z = inputs["Ring"]["Zr"]
    R.J = inputs["Ring"]["Jr"]
    R.N = inputs["Ring"]["Nrng"]
    
    L.F = inputs["Load"]["F"]
    L.M = inputs["Load"]["M"]
    L.V = inputs["Load"]["V"]
    L.Pa = inputs["Load"]["Pa"]
    L.Ph = inputs["Load"]["Ph"]
    L.sf = inputs["Load"]["sf"]
    L.sfp = inputs["Load"]["sfp"]
    
    XL[0] = inputs["XL"]["h_min"]
    X[0] = inputs["DV"]["h"]
    XU[0] = inputs["XU"]["h_max"]
    # Tol[0] = finput(in)
    
    XL[1] = inputs["XL"]["l2_min"]
    X[1] = inputs["DV"]["l2"]
    XU[1] = inputs["XU"]["l2_max"]
    # Tol[1] = finput(in)
    
    XL[2] = inputs["XL"]["tst_min"]
    X[2] = inputs["DV"]["tst"]
    XU[2] = inputs["XU"]["tst_max"]
    # Tol[2] = finput(in)
    
    XL[3] = inputs["XL"]["Nst_min"]
    X[3] = inputs["DV"]["Nst"]
    XU[3] = inputs["XU"]["Nst_max"]
    # Tol[3] = finput(in)
    
    XL[4] = inputs["XL"]["t_min"]
    X[4] = inputs["DV"]["t"]
    XU[4] = inputs["XU"]["t_max"]
    # Tol[4] = finput(in)
    
    XL[5] = inputs["XL"]["W_min"]
    X[5] = inputs["DV"]["W"]
    XU[5] = inputs["XU"]["W_max"]
    # Tol[5] = finput(in)
    
    return [Title, IPRINT, METHOD, SSP, M, C, S, R, L, mflag, LEB,XL,X,XU]
# /****************************************** end readinput ****/


def pinput( S, R, M, L,  C, IPRINT, METHOD, out, SSP, X,  XL,  XU, mflag, LEB):
    print('\n\n************************ INPUT VALUES *****************',file=out)
    print('\nFLAGS',file=out)
    print('\n IPRINT = %d',IPRINT,file=out)
    
    if int(IPRINT) == 0: 
        print("    No screen output by DOT",file=out) 
        #break
    elif int(IPRINT) == 1: 
        print("    initial and final DOT output to screen",file=out) 
        #break
    elif int(IPRINT) == 2:
        print("    initial/final + OBJ & X[] output to screen",file=out)
        #break
    elif int(IPRINT) == 3:
        print("    initial/final + OBJ & X[] + G[] output to screen",file=out)
        #break
    elif int(IPRINT) == 4:
        print("    init./final + OBJ & X[] + G[] + grads. to screen",file=out)
        #break
    elif int(IPRINT) == 5:
        print("    init./final + OBJ & X[] + G[] + grads. to screen",file=out)
        #break
    elif int(IPRINT) == 6:
        print("    init./final+OBJ & X[]+G[]+grads.+S+X scales & S info",file=out)
        #break
    elif int(IPRINT) == 7:
        print("    init./final+OBJ & X[]+G[]+grads.+S+X scales & S info",file=out)
    else:
        print("**INVALID FLAG, NO SCREEN OUTPUT DURING OPTIMIZATION",file=out)
        #break
    # # end switch
    
    print('\n METHOD = %d',METHOD)
    if (2 == METHOD):
        print("    Sequential Linear Programming",file=out)
    else:
        print("    Modified Method of Feasible Directions",file=out)
    print("\n SSP = %d ",SSP,file=out)

    if int(SSP) == 0: 
        print("    Final CORSS output only",file=out) 
        #break
    if int(SSP) == 1: 
        print("    Final calculations and final output",file=out) 
        #break
    if int(SSP) == 2: 
        print("    Optimization history and final output",file=out)
        #break
    if int(SSP) == 3: 
        print("    Optimization history, final calculations and",file=out)
        print(" final output",file=out)
    # end switch
    print('\n\nMaterial Properties',file=out) 
    print("\n    nu     =  %-6.3f                Poisson's Ratio",M.nu,file=out) 
    print("\n    E      =  %-11.3E          Young's Modulus",M.E,file=out) 
    print("\n    SM     =  %-11.3E          Shear Modulus",M.G,file=out) 
    print("\n    rho    =  %-6.3f               Density",M.rho,file=out) 
    print("\n    Stu    =  %-8.1f            Ultimate Tensile Stress",M.Stu,file=out) 
    print("\n    Scy    =  %-8.1f            Yield Compressive Stress",M.Scy,file=out) 
    
    print('\n\nCylinder Geometry',file=out) 
    print('\n    r    =  %-7.2f                   Radius',C.r,file=out) 
    print("\n    l    =  %-7.2f                   Length",C.l,file=out) 
    print("\n    fwt  =  %-7.2f            Additional Weight",C.fwt,file=out) 
    
    print("\n\nStringer Parameters",file=out) 
    print("\n    stype   =  %c                         ",S.stype,file=out)
    
    if (S.stype == 'H'):
        print('Hat Stringers\n Local Elastic Buckling',file=out)
        if ('N'==LEB):
            print(" NOT ALLOWED",file=out) 
        else:
            print(" IS ALLOWED",file=out)
            print("\n   alp  = %-5.1f      Leg angle",S.alp,file=out)
            print("\n   l1   = %-6.3f      Stringer/skin length",S.l1,file=out)
        # end if
    else:
        print( "I Stringers\n    Local Elastic Buckling",file=out)
        if ('N' == LEB):
            print(" NOT ALLOWED",file=out)
        else:
            print(" IS ALLOWED")
        print("\n    increase m from 1",file=out)
        if(0 == mflag):
            print(" while coupled buckling stress decreases",file=out)
        else:
            print(" to %d",mflag,file=out)
        print("\n   alp   = %-5.3f        Web Angle",S.alp,file=out)
        print("\n   l1    = %-4.3f        Bottom flange thickness",S.l1,file=out)
    # end if
    print("\n    MZs   = %-2.0f           Stringers ",S.MZs,file=out)
    if (-1 == S.MZs):
        print("internal",file=out)
    else:
        print("external",file=out)
    
    print("\n\nRing Parameters",file=out)
    if (0 == R.N):
        print("\n    No Rings",file=out)
    else:
        print("\n    Ar    = %-7.4f       Cross sectional area",R.A,file=out)
        print("\n    Ir    = %-8.5f       Moment of inertia",R.I,file=out)
        print("\n    Zr    = %-7.4f       Neutral Axis Distance",R.Z,file=out)
        print("\n    Nrng  = %-2.0f       Number of Rings",R.N,file=out)
    # end if
    print("\n\nLoads",file=out)
    print("\n    F  = %-9.1f          Axial Compression",L.F,file=out)
    print("\n    M  = %-11.4G         Bending Moment",L.M,file=out)
    print("\n    V  = %-8.1f          Shear force",L.V,file=out)
    print("\n    Pa  = %-6.3f         Axial pressure component",L.Pa,file=out)
    print("\n    Ph  = %-6.3f         Hoop pressure component",L.Ph,file=out)
    print("\n    sf  = %-5.2f         Safety factor",L.sf,file=out)
    print("\n    sfp = %-5.2f         Plate buckling safety factor",L.sfp,file=out)
    
    print("\n\nDesign Variables",file=out)
    print("\nVar.      Minimum      Initial      Maximum      Name",file=out)
    print("\nh  %14.4f  %12.4f  %12.4f      Stringer Height",XL[0],X[0],XU[0],file=out)
    print("\nl2 %13.4f  %12.4f  %12.4f      Top flange ",XL[1],X[1],XU[1],file=out)
    if ('H' == S.stype):
        print('length',file=out) 
    else:
        print('thickness',file=out)
    print("\ntst  %12.4f  %12.4f  %12.4f ",XL[2],X[2],XU[2],file=out)
    
    if ('H' == S.stype):
        print('Stringer thickness',file=out)
    else:
        print('Web thickness',file=out)

    print('\nNst %12.1f %12.1f %12.1f      Number of stringers',XL[3],X[3],XU[3],file=out)
    print('\nt %12.4f  %12.4f  %12.4f      Skin thickness',XL[4],X[4],XU[4],file=out)
    print("\nW %12.4f  %12.4f  %12.4f      ",XL[5],X[5],XU[5],file=out)
    if ('H' == S.stype):
        print('Top Flange Thickness',file=out)
    else:
        print('Stringer Width',file=out)
    print("\n\n",file=out)
    #fflush(out)
    #out.flush()
#/**************************** end pinput ****/


def finalout( R, S, M, L, C,  G, gamF, gamM, GCB, mgcb, N, ncr, ngcb, Nx, obj, out, Pcrush, Scol, Scrip1, Scrip2, Scrstcol, LEB, mdrv, Io, Faxial):
    print('F')
    print("\n\n------------------------------------------------------------",file=out)
    print("----------",file=out)
    print("\n\nCylinder Weight = %3.1f\n (Skin:",obj,file=out)
    print(" %3.1f, Stringers: %3.1f", (math.pi*2*C.r*C.t*C.l*M.rho), (S.A*C.l*S.N*M.rho),file=out)
    print(", Rings: %3.1f, Flanges: %3.1f)", (R.A*R.N*2*math.pi*(C.r+R.Z)*M.rho),C.fwt,file=out)
    print("\n\nh = %6.4f, Stringer Height",S.h,file=out)
    print("\nl2 = %6.4f, ",S.l2,file=out)
    if (S.stype == 'H'):
        print('Hat stringer top flange length',file=out)
    if (S.stype == 'I'):
        print('I top flange thickness',file=out)
    print("\ntst = %6.4f, ",S.t,file=out)
    if (S.stype == 'H'):
        print('Hat stringer thickness',file=out)
    if (S.stype == 'I'):
        print("I stringer web thickness",file=out)
    print("\nNst = %6.1f, Number of Stringers, b = %6.4f",S.N,S.b,file=out)
    print("\nt = %6.4f, Skin Thickness",C.t,file=out)
    print("\nW = %6.4f, ",S.W,file=out)
    if ('H' == S.stype):
        print('Hat stringer top flange thickness',file=out)
    if ('I' == S.stype):
        print("I stringer width",file=out)
    print( "\n\nStringer: I = %f, J = %f, Z = %f, A = %f",S.I,S.J,S.Z,S.A,file=out)
    print( "\nEnd Ring I should be at least ",file=out)
    print( "%g", .172*Scrstcol*(S.A+S.b*C.t)*C.r*C.r*C.r/R.d/M.E ,file=out)
    print( "\nEnd Ring Area should be at least",file=out)
    print( " %g*(r+Z)",4*math.pi*math.pi*S.I*C.r/S.b/pow(R.d,3),file=out)
    print( "\n\t\t\t\t\t\t\t\t G[] value",file=out)
    print( "\nSkin: (Shear ratio)**2 + Stress ratio\t\t = %7.5f < 1 %11.5f",G[0]+1,G[0],file=out)
    print('\n\nApplied Column Stress (SF = %4.2f) = %11.1f',L.sf,Scol,file=out)
    
    if ('I'==S.stype):
        print("\n Critical Coupled Buckling Stress (m = %3d)   = %8.1f",mdrv, Scrip2,file=out)
        print("%15.5f",G[4])
        if ('N'==LEB):
            print("\n Critical Flange Elastic Buckling Stress    = %8.1f",Scrip1,file=out)
        else:
            print("\n Critical Stringer Crippling Stress       = %8.1f",Scrip1,file=out)
        print("%15.5f",G[3])
    
    else:
        if ( 'N' ==LEB):
            print("\n Critical Local Buckling Stress (flange)    = %8.1f",Scrip1,file=out)
            print('%15.5f',G[3],file=out)
            print("\n Critical Local Buckling Stress (web)    = %8.1f",Scrip2,file=out)
            print("%15.5f",G[4],file=out)
        else:
            print("\n Critical Stringer Crippling Stress    = %8.1f",Scrip1,file=out)
            print('%15.5f',G[3],file=out)
    # end if
    
    print("\n    Critical Column Stress ",file=out)
    if ('N' == GCB):
        print("            = %8.1f",Scrstcol,file=out)
        print('%15.5f',G[1],file=out)
    
    else:
        print("controlled by General Cylinder Buckling",file=out)
    print("\n\nApplied Von Mises Stress (SF = %4.2f)    = %11.1f",L.sf, (G[2]+1)*M.Scy,file=out)
    print("\n Yield Compressive Stress        = %8.1f",M.Scy,file=out)
    print('%15.5f',G[2],file=out)
    if ('N' == GCB):
        print("\n\nGeneral Cylinder Buckling controlled by ",file=out)
        print("Critical Column Buckling",file=out)
    else:
        print("\nknCylinder: Line Load ratio + Pressure ratio",file=out)
        print("      = %7.5f < 1 %11.5f",G[1]+1,G[1],file=out)
        print("\nApplied Cylinder Line Load (SF = %4.2f)    = %11.1f",L.sf, (L.M*L.sf*C.r/Io*(S.A/S.b+C.t) + Faxial*(S.A/S.b+C.t)),file=out)
        print("\nKnockdown Adjusted Line Load               = %11.1f",N,file=out)
        print("\n    Critical General Cylinder Buckling Line Load = %8.1f",Nx,file=out)
        print("\n    Axial half waves, m = %i, Hoop waves, n = %i",mgcb, ngcb,file=out)
        print("\n    Axial Knockdown Factor, gammaF = %6.4f",gamF,file=out)
        print("\n    Bending Knockdown Factor, gammaM = %6.4f",gamM,file=out)
        if ( (mgcb != 1) and (mgcb == R.N+1) ):
            print("\nAxial half waves = Number of Cylinder segments",file=out)
            print("\n Rings may not support general buckling",file=out)
    # end if
    
    if (L.Ph < 0.0):
        print("\nApplied Crushing Hoop Pressure (SF = %4.2f)    = %16.3f",L.sf,-L.Ph*L.sf,file=out)
        print("\n    Critical Buckling Pressure       = %8.3f",Pcrush,file=out)
        print("\n Hoop waves, n = %i",ncr,file=out)
    # end if
    
    if ((Nx*(S.A/S.b+C.t)) > M.Scy):
        print("\n    If load increases over limit load, critical",file=out)
        print("\n       buckling will decrease due to plasticity.",file=out)
    # end if
# /******************************** end finalout ***/