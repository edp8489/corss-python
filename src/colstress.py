#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
file: colstress.py

Created on Sun Nov  6 20:23:26 2016

@author: eric
"""

#include "ct.h"
#import ct
import math
import numpy as np

# return[Anew, I, We0, ybar]
def newI(Acyl, Anew, S, M, sf, t, E, Faxial, I, Io, out, Scrpl, SSP, We0, ybar, Ysk, Yst):
    #float A, Slb, Ssk[541+1],Sst[541+1],sumA, sumAy,sumAyy,We[541+1],tmp;
    Ssk = np.zeros(542,dtype=float)
    Sst = np.zeros(542,dtype=float)
    We = np.zeros(542,dtype=float)
    #int i,N;
    N = S.N/2;
    Slb = .9*Scrpl;
    if (SSP == 6):
        print("\n\nStress Carried in Buckled Skin, Slb = %7.1f",Slb,file=out);
        print("\n\n   I      Y-Stringer         Y-skin      Stress-str.",file=out);
        print("     Stress-skin       Eff. Width",file=out);
    # end if
    
    for i in range(0,N/2+1):
        Sst[i] = Faxial*Acyl/(Anew) + M*sf/(I)*(Yst[i]+(ybar));
        Ssk[i] = Faxial*Acyl/(Anew) + M*sf/(I)*(Ysk[i]+(ybar));
        We[i] = .85*t*math.sqrt(E/Sst[i])
        
        if (Sst[i] < 0):
            We[i] = S.b;
        elif ( We[i] > S.b ):
            We[i] = S.b;
        if (SSP == 6):
            print("\n%3i %13.4f %13.4f %13.1f %13.1f %13.4f",i,Yst[i],Ysk[i],Sst[i],Ssk[i],We[i],file=out);
    # end for
    
    A= 0;
    sumA = 0;
    sumAy = 0;
    sumAyy = 0;
    We[N] = We[0];

    if (SSP == 6):
        print("\n\n   I        Area        sum Area        sum Area*y",file=out);
        print("    sum Area*y*y skin/str.",file=out);
    # end if
    
    for i in range(0,N/2):
        if ( (S.stype == 'H') and (Sst[i] > 0) ):
            tmp = S.l2+2*S.la+S.l1
            if ( tmp  > (2*We[i]) ):
                A = 2*( t*(Sst[i] - Slb)/Sst[i]*(tmp-2*We[i]) );
                sumA += A;
                sumAy += A*Yst[i];
                sumAyy += A*Yst[i]*Yst[i];
                if (SSP == 6):
                    print("\n%3i %15.3f %15.3f %15.3f %15.3f stringer",i,A, sumA, sumAy,sumAyy,file=out);
            #end if
        if (Ssk[i] > 0):
            if (S.stype == 'H'):
                tmp=S.b-(S.l2+2*S.la+S.l1)
                if ( tmp > (We[i] + We[i+1]) ):
                    A = 2*( t*(Ssk[i] - Slb)/Ssk[i]*(tmp-We[i]-We[i+1]) );
                    sumA += A;
                    sumAy += A*Ysk[i];
                    sumAyy += A*Ysk[i]*Ysk[i];
                    if (SSP == 6): 
                        print('\n%3i %15.3f %15.3f %15.3f %15.3f skin',i, A, sumA, sumAy, sumAyy,file=out) ;
                # end if
            #end if
            elif ( S.b > (We[i]+We[i+1]) ):
                A = 2*( t*(Ssk[i] - Slb)/Ssk[i]*(S.b-We[i]-We[i+1]) ) ;
                sumA += A;
                sumAy += A*Ysk[i];
                sumAyy += A*Ysk[i]*Ysk[i];
                if (SSP == 6):
                    print("\n%3i %15.3f %15.3f %15.3f %15.3f skin",i, A, sumA, sumAy, sumAyy,file=out) ;
            #end if
        # end if
    # end for
    We0 = We[0];
    Anew = Acyl-sumA;
    ybar = sumAy/(Anew);
    I = Io - sumAyy - (Anew)*(ybar)*(ybar);
    if (SSP==6):
        print("\n\n Ybar = %7.3f,    Cylinder I = %9.1f,    Anew = %9.1f",ybar, I, Anew,file=out);
    # end if
    return [Anew, I, We0, ybar]
# /************************ end newI ****/

# return [Anew, St, We0]
def loopI(Anew, S, M, sf, E, C, Faxial, Io, out, St, Scrpl, SSP, We0, Ysk, Yst):
    #float Acyl, I, Iold, ybar;
    #int count;
    count = 0;
    ybar = 0;
    I = Io/2;
    Iold = Io;
    Acyl = 2*math.pi*(C.r-C.t/2)*C.t+S.N*S.A;
    Anew = Acyl;
    
    while( abs((Iold-I)/Iold) > .001 ):
        Iold = I;
        count += 1
        [Anew, I, We0, ybar] = newI(Acyl,Anew, S,M, sf,C.t,E,Faxial,I,Io,out,Scrpl,SSP,We0,ybar,Ysk,Yst);
        if (count == 20):
            print("\nDid not converge on a Cylinder I in 20 iterations,",file=out);
            print(out," I = %2.0f, I set to 1/2 Io =",I,file=out);
            I = Io/2;
            print(" %2.0f, t = %6.4f, Nst = %5.1f",I,C.t,S.N,file=out);
            break;
        # end if
    # end while
    St = -M*sf*(C.r-ybar)/I + Faxial*Acyl/(Anew);
    if (SSP == 4):
        print("\n\nNumber of iterations to converge on I, %d",count,file=out);
        print('\n\nTension load (adjusted for buckled skin) = %8.1f',St,file=out);
        [Anew, I, We0, ybar] = newI(Acyl,Anew, S,M, sf,C.t,E, Faxial,I,Io,out,Scrpl,6,We0,ybar,Ysk,Yst);
    # end if

    return [St, Anew, St, We0]
# /********** end loopI ****/



# return [Anew, St, Scol, Scrstcol]
def colstress(Anew, S, d, E, L, C, Faxial, Io, out, St, Scol, Scrip, Scrpl, Scrstcol, Sskbd, SSP, taucr, tauskbd, Ysk, Yst):
    #float be, Ise,radg,ScrstcolJE,We0,tmp;
    print('C');
    Scol = L.M*L.sf*C.r/Io + Faxial;
    be = S.b;
    if (L.sfp < L.sf):
        if (L.sf/L.sfp*Sskbd/Scrpl+pow(L.sf/L.sfp*tauskbd/taucr,2) > 1):
            [Scol, Anew, St, We0] = loopI(Anew, S,L.M,L.sf,E,C,Faxial,Io,out,St,Scrpl,SSP,We0,Ysk,Yst);
            if (S.stype == 'H'):
                tmp=S.l1+S.l2+2*S.la
                if ( tmp < 2*We0 ):
                    be=tmp; #/* between legs */
                else:
                    be=2*We0;
                if ( (S.b-S.l1-2*S.la-S.l2) < 2*We0 ):
                    be += S.b-S.l1-2*S.la-S.l2
                else:
                    be += 2*We0;
            elif ( S.b > 2*We0 ):
                be = 2*We0;
        # end if
    # end if
    Ise = be*C.t*C.t*C.t/12. + S.I + S.A*S.Z*S.Z - S.A*S.A*S.Z*S.Z/(S.A+be*C.t);
    if (Ise<0):
        Ise = 0.00001;
    
    radg = math.sqrt(Ise/(S.A+be*C.t));
    
    if (d/radg <= math.pi*math.sqrt(2*E/Scrip)):
        Scrstcol = Scrip - Scrip*Scrip/4/math.pi/math.pi/E*d*d/radg/radg;
        if (SSP == 4):
            print("\n\nJohnson-Euler Column Buckling (Scrip = %g) = %g",Scrip,Scrstcol,file=out);
    # end if
    else:
        Scrstcol = pow(math.pi*radg/d,2)*E;
        if (SSP == 4):
            print('\n\nEuler Column Buckling = %g',Scrstcol,file=out);
    # end else
    if (SSP == 4):
        print("\nApplied Stress = %8.1f",Scol,file=out);
        print("\nEffective skin width on stringer with max stress = %f",be,file=out);
        print("\nStringer+Skin I = %6.4f, radius of gyration = %6.4f",Ise,radg,file=out);
    # end if
    
    return [(Scol/(Scrstcol) -1. ), Anew, St, Scol, Scrstcol]
# /************************ end colstress ****/

