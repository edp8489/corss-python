#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
file: eval.py

Created on Sun Nov  6 19:11:25 2016

@author: eric
"""

#include "ct.h"
import ct
#include "skinbuck.h"
import skinbuck
#include "lebcrip.h"
import lebcrip
#include "colstress.h"
import colstress
#include "gencyl.h"
import gencyl
#include "stress.h"
import stress
#include "cio.h"
import cio
import stringer

import math
import numpy as np

def weight(C,S,R,M):
    w = (math.pi*2*C.r*C.t*C.l + S.A*C.l*S.N + R.A*R.N*2*math.pi*(C.r + R.Z))*M.rho + C.fwt
    return w
# end weight


# return [OBJ]
def eval( S, R, M, L, C,  G, OBJ, out, SSP, mflag, LEB):
    #float Anew, dtheta, Faxial, Faxialp, Io, N, Nx, Pcr, St,
    #    Scol, Scrip1, Scrip2, ScripS, Scrp1, Scrstcol,
    #    Ssk, Sskbd, tau, tau0, taucr, tauskbd, theta, Ysk[541], Yst[1081],
    #    gamF, gamM;
    #int i, mgcb, ncr, ngcb, mdrv;
    Ysk = np.zeros(541)
    Yst = np.zeros(1081)
    #char GCB;
    print('\n');
    print('E');
    if (S.N > 1080):
        print("\n\nNumber of stringers exceeds 1080, ");
        print("Number of stringers set to 1080");
        S.N = 1080;
        if (4 == SSP):
            print("\n\nNumber of stringers exceeds 1080, ",file=out);
            print("Number of stringers set to 1080",file=out);
        #end if
    # end if
    if (SSP == 4):
        print('\n\nSTRINGER PROPERTY CALCULATIONS',file=out);
    #end if
    
    S = stringer(S,C,out,SSP);
    S.Z *= S.MZs;
    R.d = C.l/(R.N+1);
    S.b = 2*math.pi*C.r/S.N;
    Anew = 2*math.pi*C.r*C.t + S.A*S.N;
    if (L.Pa<0):
        Faxial = (L.F-L.Pa*math.pi*C.r*C.r)*L.sf/Anew;
        Faxialp = (L.F-L.Pa*math.pi*C.r*C.r)*L.sfp/Anew;
    #end if
    else:
        Faxial = (L.F*L.sf-L.Pa*math.pi*C.r*C.r)/Anew;
        Faxialp = (L.F*L.sfp-L.Pa*math.pi*C.r*C.r)/Anew;
    # end else
    Io = math.pi*C.t*C.r*C.r*C.r + S.N*S.I;
    theta = 0;
    dtheta = 2*math.pi/S.N;

    for i in range(0,(S.N/2+1)):
        Yst[i] = (C.r+S.Z)*math.cos(theta);
        Ysk[i] = C.r*math.cos(theta);
        Io += 2*S.A*Yst[i]*Yst[i];
        theta += dtheta;
    # end for
    St = -L.M*L.sf*C.r/Io + Faxial; # tension stress 
    if (SSP == 4):
        print("\n\nOVERALL CYLINDER CALCULATIONS",file=out);
        print("\n\nIo = %-22.0f   Tension Stress = f(Io) = %2.0f",Io,St,file=out);
        print("\nCross sectional area = %f",Anew,file=out);
        print("\nFaxial = f(F, Pa) = %8.1f   Faxialp = %8.1f",Faxial,Faxialp,file=out);
        print("\nRing Spacing = %-12.4f   Stringer Spacing = %7.4f",R.d,S.b,file=out);
        print("\n\nSKIN BUCKLING STRESS AND MAX SHEAR CALCULATIONS",file=out);
    # end if
    #G[0] = skinbuck(S,R.d,M,L,C,Faxialp, Io, out,&Scrp1,&Sskbd, SSP,&Ssk,&tau,&tau0,&taucr,&tauskbd,Ysk,Yst);
    [Scrpl, Sskbd, Stsk,tau,tau0,taucr,G[0]] = skinbuck(S,R.d,M,L,C,Faxialp, Io, out,Scrp1,Sskbd, SSP,Ssk&tau,tau0,taucr,tauskbd,Ysk,Yst)
    if (SSP == 4):
        print("\n\nSTRINGER CRIPPLING CALCULATIONS\n",file=out);
    [Scrip1,Scrip2, ScripS, mdrv] = getScrip(S,M, out,Scrip1,Scrip2,ScripS,SSP,mdrv, C.l,mflag,LEB);
    if (SSP == 4):
        print('\n\nSTRINGER COLUMN BUCKLING CALCULATIONS',file=out);
    [G[1],Anew, St, Scol, Scrstcol] = colstress(Anew, S,R.d,M.E,L,C,Faxial,Io,out,St,Scol,ScripS,Scrp1,Scrstcol,Sskbd,SSP, taucr, tauskbd,Ysk,Yst);
    GCB = 'N';
    G[3] = Scol/Scrip1 -1.;
    G[4] = Scol/Scrip2 -1.;
    if (Scol<0):
        print("\n\n\nWhole Cylinder is in Tension due to pressure, this",file=out);
        print(" is not a buckling problem.\n\n",file=out);
        exit(1);
    #end if
    if (SSP == 4):
        print("\n\nGENERAL CYLINDER BUCKLING CALCULATIONS",file=out);
    [G[1], mgcb, ngcb, Nx, Pcr, N, ncr, GCB, gamF, gamM] = GCBcalc(R,S,M,C,L,G,out,SSP,mgcb,ngcb,Nx,Pcr,N, Io,Faxial,ncr,GCB,gamF,gamM);
    if (4 == SSP):
        print("\n\nVON MISES STRESS CHECK\n",file=out);
    G[2] = stresscheck(Anew,S.A,S.N,L.Ph,C.r,C.t,Faxial,out,St,Scol,SSP,tau,tau0,
                        L.sf)/M.Scy - 1.;
    #*OBJ = (pi*2*C.r*C.t*C.l + S.A*C.l*S.N + R.A*R.N*2*pi*(C.r + R.Z))*M.rho + C.fwt;
    OBJ = weight(C,S,R,M)    
    if ( (SSP == 4) or (SSP == 5)):
        finalout(R,S,M,L,C,G,gamF,gamM,GCB,mgcb,N,ncr,ngcb,Nx,OBJ,out,Pcr,Scol,Scrip1,Scrip2,Scrstcol,LEB,mdrv,Io,Faxial);
    # end if
# /*********************************** end eval ***/