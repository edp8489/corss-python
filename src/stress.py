#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
file stress.py

function VMStress calculates Von Mises stress

function stresscheck calculates compression, shear, and tenstion and returns
the highest value

Created on Fri Nov  4 18:54:51 2016

@author: eric
"""
import math
#include "ct.h"

#float VMStress(char loc[], FILE *out, int SSP, float Sx, float Sy, float tauxy)
def VMStress(loc, out, SSP, Sx, Sy, tauxy):
    S1 = (Sx+Sy)/2 + math.sqrt( (Sx-Sy)*(Sx-Sy)/4 + tauxy*tauxy );
    S2 = (Sx+Sy)/2 - math.sqrt( (Sx-Sy)*(Sx-Sy)/4 + tauxy*tauxy );
    VMS = math.sqrt( ( (S1-S2)*(S1-S2) + S2*S2 + S1*S1 )/2 );
    if SSP == 4:
        print('\nPoint of %s:',loc,file=out);
        print('\nSx = %8.1f,    Sy = %8.1f,    tauxy = %8.1f',Sx,Sy,tauxy,file=out);
        print('\nSl = %8.1f,    S2 = %8.1f,    Von Mises Stress = %8.1f',S1,S2,VMS,file=out);
    #end if
    
    return (VMS);
""" /******************** end VMstress ****/ """


#float stresscheck(float Anew, float As, float Ns, float Ph, float r, float t,
#                float Faxial, FILE *out, float St, float Scol, int SSP, float tau,
#                float tau0, float sf)
def stresscheck(Anew, As, Ns, Ph, r, t, Faxial, out, St, Scol, SSP, tau, tau0,  sf):
    print('S');
    VMSA = VMStress('Max Compression',out,SSP, Scol,-Ph*r/t*sf,tau0);
    VMSB = VMStress('Max Shear',out,SSP, Faxial*(2*math.pi*(r-t/2)*t+As*Ns)/Anew,-Ph*r/t*sf,tau);
    VMSC = VMStress('Max Tension',out,SSP, St,-Ph*r/t*sf,tau0);
    if VMSB > VMSA:
        VMSA = VMSB;
    if VMSC > VMSA:
        VMSA = VMSC;
    return(VMSA);
""" /************** end stresscheck ****/ """




