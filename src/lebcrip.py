#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

file lebcrip.py
Created on Fri Nov  4 21:42:05 2016

@author: eric
"""

#include "ct.h"
import math

#void domainexit(float l2, float l1, float h, FILE *out)
def domainexit(l2, l1, h, out):
    print('\a\a\n\nTop flange thickness + bottom flange thickness');
    print(" > stringer height");
    print('\n%20f + %23f > %15f',l2,l1,h);
    print('\nThis will cause a SQRT domain error, please make sure');
    print('\nthe smallest Stringer Height is greater then the');
    print('\nBottom Flange Thickness plus the largest Top Flange');
    print(' Thickness');
    print(out,'\n\nTop flange thickness + bottom flange thickness',file=out);
    print(out," > stringer height",file=out);
    print(out,'\n%20f + %23f > %15f',l2,l1,h,file=out);
    print(out,'\nThis will cause a SQRT domain error, please make sure',file=out);
    print(out,'\nthe smallest Stringer Height is greater then the',file=out);
    print(out,'\nBottom Flange Thickness plus the largest Top Flange',file=out);
    print(out,' Thickness');
    #closeall();
    exit(2);
# /*************************** end domainexit ****/



#float G6eqn(float S, float m, float l, float tst, float nu, float h,
#            float l2, float W, float E)
def G6eqn(S, m, l, tst, nu, h, l2, W, E):
    #float M, Nx, D, k1,alpha,beta,b,I,Ab, k2,k3;
    M = m*m*math.pi*math.pi/l/l;
    Nx = S*tst;
    D = E*tst*tst*tst/12/(1-nu*nu);
    k1 = math.sqrt(Nx*M/D);
    alpha = math.sqrt(M+k1);
    beta = math.sqrt(-M+k1);
    b = h-l2;
    I = l2*W*W*W/12;
    Ab = W*l2;
    k2 = E*I*math.sin(beta*b)*M*M;
    k2 += D*(( beta*beta*beta+beta*(2-nu)*M)*math.cos(beta*b));
    k2 -= Ab*S*math.sin(beta*b)*M;
    k2 *= (alpha*alpha-nu*M)*math.sinh(alpha*b);
    k3 = E*I*math.sinh(alpha*b)*M*M;
    k3 -= D*((alpha*alpha*alpha-alpha*(2-nu)*M)*math.cosh(alpha*b));
    k3 -= Ab*S*math.sinh(alpha*b)*M;
    k3 *= (beta*beta+nu*M)*math.sin(beta*b);

    rtn = k2 + k3
    return rtn
# /***************************** end G6eqn ***/


#float secant(int m, float l, float tst, float nu, float h, float l2, float W,
#            float E)
def secant(m, l, tst, nu, h, l2, W, E):
    #float Fj,hold,Fi,Smin,Smax, Si,Sj;
    #int i,count;
    count=0;
    Smin = Sj = m*m*math.pi*math.pi*E*tst*tst/l/l/12/(1-nu*nu)+1.;
    Fj = G6eqn(Sj,float(m),l,tst,nu,h,l2,W,E);
    Si = 2*Sj;
    Fi = G6eqn(Si, float(m),l,tst,nu,h,l2,W,E);
    while (((Fi<0) and (Fj<0)) or ((Fi>0) and (Fj>0))):
        Si *= 2;
        Fi = G6eqn(Si, float(m),l,tst,nu,h,l2,W,E);
    #end while
    Sj = Si/2;
    Smax = Si;
    Fj = G6eqn(Sj, float(m),l,tst,nu,h,l2,W,E);
    while True:
        if ( Si<Smin ):
            Si=Smax;
        hold = Si;
        Fi = G6eqn(Si, float(m),l,tst,nu,h,l2,W,E);
        if ((Si==Sj) or (Fi==Fj)):
            break; 
        else:
            Si = Sj - Fj/( (Fi-Fj)/(Si-Sj));
        Sj = hold;
        Fj = Fi;
        if ((count+1) > 20):
            break;
        if not (abs(Fi) > .001 ):
            break
    # end do-while
    return Si
# /****************************** end secant ****/



#float findICBuc(int mflag, int *mdrv, float l, float tst, float nu, float h,
#                float l2, float W, float E, int SSP, FILE *out)

def findICBuc(mflag, mdrv, l, tst, nu, h,l2, W, E, SSP, out):
    #float Sdrv, S,Slast;
    #int i,m;
    m=0;
    S = 1E30;
    if (mflag==0):
        while True:
            m +=1;
            Slast = S;
            S = secant(m,l,tst,nu,h,l2,W,E);
            if (4==SSP):
                print("\nm = %d, Critical Stress = %f",m,S,file=out);
            if not  ((S-Slast)<0):
                break
        # end do-while
        #*mdrv = m-1;
        mdrv = m-1
        Sdrv = Slast;
    # end if
    else:
        Sdrv = S;
        #for (i=1; i<=mflag; i++)
        for i in range(1,mflag):
            S = secant(i,l,tst,nu,h,l2,W,E);
            if (S<Sdrv):
                Sdrv=S;
                #*mdrv=i; }
                mdrv=i
            if (4==SSP):
                print('knm = %d, Critical Stress = %f',i,S,file=out);
        #end for
    #end if
    return [Sdrv,mdrv]; #return mdrv as well?
# /*************** end findICBuc *****/


#float getScrip(struct stringer S, struct material M, FILE *out,
#                float *Scrip1, float *Scrip2, float *ScripS, int SSP, int *mdrv,
#                float l, int mflag, char LEB)
def getScrip(S, M, out,Scrip1, Scrip2, ScripS, SSP, mdrv, l, mflag, LEB):
    #float S1,S2,S3,S4,alpha;
    #int m;
    print('C');
    alpha = math.radians(S.alp)
    if (S.stype == 'I'):
        S2 = findICBuc(mflag,mdrv, l,S.t,M.nu,S.h,S.l2,S.W,M.E,SSP,out);
        if (4==SSP):
            print('\nCoupled Buckling Stress, %9.2f',S2,file=out);
        S3 = .569311 / pow(math.sqrt(M.Scy/M.E*(S.W/2)/S.l2), .812712 ) * M.Scy;
        if ((S.h-S.l2-S.l1) <= 0):
            domainexit(S.l2,S.l1,S.h, out);
        S4 = 1.387194/pow(math.sqrt(M.Scy/M.E*((S.h-S.l2-S.l1)/math.cos(alpha))/S.t),.807179)* M.Scy;
        if (S3 > M.Stu):
            S3 = M.Stu;
        if (S4 > M.Stu):
            S4 = M.Stu;
        #*ScripS = S1 = ( S3*S.l2*S.W + S4*S.t*((S.h-S.l2-S.l1)/cos(alpha)) )/ ( S.A - S.l1*S.W );
        ScripS = S1 = ( S3*S.l2*S.W + S4*S.t*((S.h-S.l2-S.l1)/math.cos(alpha)) )/ ( S.A - S.l1*S.W )
        if (4==SSP):
            print('\nCrippling: Top Flange = %8.1f, Web = %8.1f',S3,S4,file=out);
        if ('N' == LEB ):
            S1 = .456*math.pi*math.pi/12*M.E/(1-M.nu*M.nu)*pow(S.l2/(S.W/2),2);
            if (4==SSP):
                print('\nFlange Elastic Buckling Stress, %9.2f',S1,file=out);
        #end if
    #end if
    else:
        if (S.l2 > S.t):
            S3 = 1.387194 / pow(math.sqrt(M.Scy/M.E*(S.l2-S.t)/S.t), .807179 ) * M.Scy;
        else:
            S3 = 0;
        S4 = 1.387194/ pow(math.sqrt(M.Scy/M.E*((S.h-S.W-S.t)/math.cos(alpha))/S.t),.807179)*M.Scy;
        if (S3 > M.Stu):
            S3 = M.Stu;
        if (S4 > M.Stu):
            S4 = M.Stu;
        #*ScripS = S1 = ( S3*S.W*S.l2 + 2*S4*S.t*((S.h-S.W-S.t)/math.cos(alpha)) ) /( S.A - S.l1*S.t*2 );
        ScripS = S1 = ( S3*S.W*S.l2 + 2*S4*S.t*((S.h-S.W-S.t)/math.cos(alpha)) ) /( S.A - S.l1*S.t*2 )
        if (4==SSP):
            print('\n\nCrippling: Top Flange = %8.1f, Web = %8.1f',S3,S4,file=out);
        if ('N'==LEB):
            if (S.l2 == 0):
                S.l2 = 1.E-6;
            S1 = 3.29*M.E/(1-M.nu*M.nu)*pow(S.W/(S.l2-S.t),2);
            S2 = 3.29*M.E/(1-M.nu*M.nu)*pow(S.t/((S.h-S.W-S.t)/math.cos(alpha)),2);
            if (4==SSP):
                print('\nCritical Local Elastic Buckling Stress:',file=out);
                print('\n Top Flange = %9.2f, Web = %9.2f',S1,S2,file=out);
        else:
            S2 = M.Stu;
    if (4==SSP):
        print('\nWeighted Average Crippling Stress, %9.2f',ScripS,file=out);
    #*Scrip1 = S1;
    Scrip1 = S1
    #*Scrip2 = S2;
    Scrip2 = S2
    # return Scrip1, Scrip2?
    return [Scrip1,Scrip2, ScripS, mdrv]
# /*************************** end getScrip ****/

